#!/bin/bash

#CPU INFO
function CPU_INFO {
echo -e "\033[32;1m------CPU INFO-------\033[0m"
echo "Number of processors: `cat "/proc/cpuinfo" | grep -c "processor"`"
for TXT in "vendor" "model name"
do
echo `cat /proc/cpuinfo | grep "$TXT" | head -1`
done
}

#SYSTEM INFO
function SYSTEM_INFO {
echo -e "\033[32;1m ------SYSTEM INFO------ \033[0m"
echo kernel-release: `uname -r`
echo operating-system: `uname -o`
KERNEL_VERSION=`uname -v |cut -d" " -f1`
echo kernel-version: ${KERNEL_VERSION:4}
echo machine hardware name: `uname -m`
}

#RAM
function RAM {
echo -e "\033[32;1m------RAM----------\033[0m"
echo "`free -h | grep "Mem"`">/tmp/myfile.txt
awk '{ print "Total memory: "$2,"\nUsed memory: " $3 }' /tmp/myfile.txt
}

#HDD INFO
function HDD_INFO {
echo -e "\033[32;1m------HARD DISK INFO------\033[0m"
echo "`lsblk -o NAME,SIZE`"
}

#NETWORK INFO
function NETWORK_INFO {
echo -e "\033[32;1m------NETWORK INFO-------\033[0m"
if [ "`ifconfig | grep eth`" ]
then 
echo "Ethernet: YES"
else
echo "Ethernet: NO"
fi
if [ "`ifconfig | grep wlan`" ]
then 
echo "Wlan: YES"
else
echo "Wlan: NO"
fi
echo -e "\033[32;1m-----------------------------------\033[0m"
}

#DISPLAY
function DISPLAY_INFO {
echo -e "\033[32;1m------DISPLAY INFO------\033[0m"
echo "`xrandr`"
echo -e "\033[32;1m-----------------------------------\033[0m"
}

#HELP
function HELP {
 clear
 echo "Usage: ./sysinfo.sh [OPTION]... "
 echo "Maximum count of options: 5 "
 echo ""
 echo "Options:"
 echo "--all: Full information about system."
 echo "--cpu: Information about CPU."
 echo "--sys: Information about system."
 echo "--mem: Information about RAM."
 echo "--hdd: Information about HDD."
 echo "--net: Information about NETWORK."
 echo "--dis: Information about DISPLAY."
 echo "--help: display this help and exit"
}

#If there are no options
if [ "$#" -eq 0 ]
then   
  echo "There is no options: type --help"
fi

#If called --help option
if [ "$1" = "--help" ]
then
 HELP
else
#If called --all option
if [ "$1" = "--all" ]
then
    CPU_INFO 
    SYSTEM_INFO 
    RAM 
    HDD_INFO 
    NETWORK_INFO
    DISPLAY_INFO
else
#If the rest are called option
while [ ! -z "$1" ]
do
 case "$1" in 
    --cpu) CPU_INFO ;;
    --sys) SYSTEM_INFO ;;
    --mem) RAM ;;
    --hdd) HDD_INFO ;;
    --net) NETWORK_INFO ;;
    --dis) DISPLAY_INFO ;;
        *) reset
           echo "$1: option not found: type --help"
           exit 1
           break;;
       
  esac
  shift
done
fi
fi

